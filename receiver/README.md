# OpenCap Receiver Service

## Getting Started

### Prerequisites

* Running Orchestrator - see `/orchestrator/README.md`
* Running TimescaleDB server (Postgres + TimescaleDB plugin)
    * With current schema - see `/db/README.md`

### Starting up

```bash
cp .env.example .env
# ... make edits ...

source .env && go run .
```

This will start a new Receiver with no running adapters\*. To start an adapter and subscribe to some trading pairs, you can use the Orchestrator admin dashboard (coming soon!) to inspect and manage all of the Receivers that are connected to Orchestrator. Alternatively, you can use a gRPC client (with the .proto file(s) found in the `/receiver/hub/api` directory) to `Start` and `SubscribePairs` on the adapter service. 

Each exchange adapter is managed by the local adapter hub on the Receiver, which is a supervisor for managing adapters, aggregating their data, and sending it to Orchestrator. Every adapter reports trade and depth data to Postgres and Redis.

> \* Adapters are essentially websocket subscribers to cryptocurrency exchanges. Whenever trade data is received, it translates the trade data to OpenCap's model and sends it to the Receiver's hub where it is then forwarded to an Orchestrator.

## Purpose

To open websocket connections to exchanges (via adapters), gather data, and send it to Orchestrator for further use.
