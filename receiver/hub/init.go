package hub

import (
	"context"
	"fmt"
	"io"
	"log"
	"time"

	"gitlab.com/mwarnerdotme/opencap/backend/env"
	orchestratorTradesAPI "gitlab.com/mwarnerdotme/opencap/orchestrator/api/trades/protos"
	"gitlab.com/mwarnerdotme/opencap/receiver/adapters"
	"gitlab.com/mwarnerdotme/opencap/receiver/adapters/binance"
	"google.golang.org/grpc"
)

type ExchangeAdapterHub struct {
	Adapters map[adapters.ExchangeAdapterLabel]adapters.ExchangeAdapter
}

func New() ExchangeAdapterHub {
	hub := ExchangeAdapterHub{}
	hub.Adapters = make(map[adapters.ExchangeAdapterLabel]adapters.ExchangeAdapter)
	return hub
}

func (hub ExchangeAdapterHub) Initialize() (err error) {
	// create channels for adapter events
	tradeChannel := make(chan *orchestratorTradesAPI.Trade)
	errorChannel := make(chan error)

	// create an orchestrator upload pump
	var tradePump []*orchestratorTradesAPI.Trade

	// connect to the orchestrator
	host := env.Getenv("ORCHESTRATOR_HOST")
	port := env.GetenvInt("ORCHESTRATOR_PORT")
	hostport := fmt.Sprintf("%s:%d", host, port)
	orchestratorConn, err := grpc.Dial(hostport, grpc.WithInsecure())
	if err != nil {
		log.Fatalf("did not connect: %v", err)
		return
	}
	orchestratorClient := orchestratorTradesAPI.NewTradeServiceClient(orchestratorConn)
	stream, err := orchestratorClient.Add(context.Background())
	if err != nil {
		log.Fatalf("did not connect: %v", err)
		return
	}

	// start handling trade pump - send trades to orchestrator
	go func() {
		for {
			if len(tradePump) > 0 {
				tradeRequest := &orchestratorTradesAPI.AddRequest{
					Trades: tradePump,
				}

				log.Printf("sending %d trades\n", len(tradePump))

				err = stream.Send(tradeRequest)
				if err != nil {
					log.Println(err)
				}

				tradePump = []*orchestratorTradesAPI.Trade{}
			}
			time.Sleep(1 * time.Second)
		}
	}()

	// handle incoming messages from stream
	go func() {
		for {
			req, err := stream.Recv()
			if err == io.EOF {
				log.Println("exit")
				return
			}
			if err != nil {
				log.Printf("receive error %v", err)
				continue
			}

			if !req.Success {
				// TODO: handle trade upload failures
			}
		}
	}()

	// instantiate adapters
	binance := binance.NewBinanceExchangeAdapter(&tradeChannel, &errorChannel)
	hub.Adapters[binance.Label()] = binance

	// listen to channels
	go func() {
		for {
			select {
			case trade := <-tradeChannel:
				// add trade to trade pump
				tradePump = append(tradePump, trade)

			case err := <-errorChannel:
				log.Printf("ERROR: %s", err)
				orchestratorConn.Close()
				// log.Println("attempting to restart the binance adapter...")
				// hub.adapters[0].Stop()
				// go hub.adapters[0].Start()
			}
		}
	}()

	return nil
}
