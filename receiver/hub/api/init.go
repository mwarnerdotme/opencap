package api

import (
	"fmt"
	"net"

	"gitlab.com/mwarnerdotme/opencap/backend/env"
	"gitlab.com/mwarnerdotme/opencap/receiver/hub"
	adaptersAPI "gitlab.com/mwarnerdotme/opencap/receiver/hub/api/adapters"
	"google.golang.org/grpc"

	receiverAdaptersService "gitlab.com/mwarnerdotme/opencap/receiver/hub/api/adapters/protos"
)

type ExchangeAdapterHubAPI struct {
	grpcServer *grpc.Server
	hub        hub.ExchangeAdapterHub
}

func New(hub hub.ExchangeAdapterHub) ExchangeAdapterHubAPI {
	api := ExchangeAdapterHubAPI{}

	// instantiate grpc server
	var opts []grpc.ServerOption
	api.grpcServer = grpc.NewServer(opts...)

	// add reflections to grpc, this allows for programs like grpcurl to work
	// reflection.Register(api)

	api.hub = hub

	return api
}

func (api ExchangeAdapterHubAPI) Initialize() (err error) {
	// init and register services
	adapterServiceServer := adaptersAPI.New(api.hub)
	receiverAdaptersService.RegisterAdapterServiceServer(api.grpcServer, adapterServiceServer)

	// start grpc server
	port := env.GetenvInt("RECEIVER_PORT")
	listener, err := net.Listen("tcp", fmt.Sprintf(":%d", port))
	if err != nil {
		return
	}

	err = api.grpcServer.Serve(listener)
	if err != nil {
		return
	}

	return
}
