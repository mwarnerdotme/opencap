package adapters

import (
	"context"

	"gitlab.com/mwarnerdotme/opencap/receiver/adapters"

	receiverAdaptersService "gitlab.com/mwarnerdotme/opencap/receiver/hub/api/adapters/protos"
)

func (server ServiceServer) Stop(ctx context.Context, in *receiverAdaptersService.StopRequest) (*receiverAdaptersService.StopReply, error) {
	adapterType := adapters.ExchangeAdapterLabel(in.Label)
	reply := receiverAdaptersService.StopReply{
		Success: false,
	}

	_, exists := server.hub.Adapters[adapterType]

	if exists {
		server.hub.Adapters[adapterType].Stop()
		reply.Success = true
	}

	return &reply, nil
}
