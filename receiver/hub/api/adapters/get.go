package adapters

import (
	"context"
	"fmt"

	"gitlab.com/mwarnerdotme/opencap/receiver/adapters"

	receiverAdaptersService "gitlab.com/mwarnerdotme/opencap/receiver/hub/api/adapters/protos"
)

func (server ServiceServer) Get(ctx context.Context, in *receiverAdaptersService.GetRequest) (*receiverAdaptersService.GetReply, error) {
	adapterLabel := adapters.ExchangeAdapterLabel(in.Label)
	adapter, exists := server.hub.Adapters[adapterLabel]

	if !exists {
		return nil, fmt.Errorf("adapter \"%s\" does not exist", in.Label)
	}

	reply := receiverAdaptersService.GetReply{
		Adapter: &receiverAdaptersService.Adapter{
			Label:  adapter.Label().String(),
			Status: adapter.Status(),
		},
	}

	return &reply, nil
}
