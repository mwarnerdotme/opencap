package adapters

import (
	"context"
	"fmt"

	"gitlab.com/mwarnerdotme/opencap/receiver/adapters"

	receiverAdaptersService "gitlab.com/mwarnerdotme/opencap/receiver/hub/api/adapters/protos"
)

func (server ServiceServer) UnsubscribePairs(ctx context.Context, in *receiverAdaptersService.UnsubscribeRequest) (*receiverAdaptersService.UnsubscribeReply, error) {
	adapterType := adapters.ExchangeAdapterLabel(in.Label)
	adapter, exists := server.hub.Adapters[adapterType]

	if !exists {
		return nil, fmt.Errorf("adapter \"%s\" does not exist", in.Label)
	}

	err := adapter.Unsubscribe(in.TradingPairs)
	if err != nil {
		return nil, fmt.Errorf("could not subscribe: %s", err)
	}

	reply := receiverAdaptersService.UnsubscribeReply{
		Success: true,
	}

	return &reply, nil
}
