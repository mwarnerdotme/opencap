package adapters

import (
	"gitlab.com/mwarnerdotme/opencap/receiver/hub"

	receiverAdaptersService "gitlab.com/mwarnerdotme/opencap/receiver/hub/api/adapters/protos"
)

type ServiceServer struct {
	receiverAdaptersService.AdapterServiceServer

	hub hub.ExchangeAdapterHub
}

func New(hub hub.ExchangeAdapterHub) (s ServiceServer) {
	s.hub = hub

	return
}
