package adapters

import (
	"context"
	"fmt"

	"gitlab.com/mwarnerdotme/opencap/receiver/adapters"

	receiverAdaptersService "gitlab.com/mwarnerdotme/opencap/receiver/hub/api/adapters/protos"
)

func (server ServiceServer) ListConnections(ctx context.Context, in *receiverAdaptersService.ListConnectionsRequest) (*receiverAdaptersService.ListConnectionsReply, error) {
	adapterType := adapters.ExchangeAdapterLabel(in.Label)
	adapter, exists := server.hub.Adapters[adapterType]

	if !exists {
		return nil, fmt.Errorf("adapter \"%s\" does not exist", in.Label)
	}

	connections := adapter.ListConnections()

	reply := receiverAdaptersService.ListConnectionsReply{
		Connections: connections,
	}

	return &reply, nil
}
