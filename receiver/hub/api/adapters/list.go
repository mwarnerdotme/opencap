package adapters

import (
	"context"

	receiverAdaptersService "gitlab.com/mwarnerdotme/opencap/receiver/hub/api/adapters/protos"
)

func (server ServiceServer) List(ctx context.Context, in *receiverAdaptersService.ListRequest) (*receiverAdaptersService.ListReply, error) {
	var adapters []*receiverAdaptersService.Adapter

	for _, adapter := range server.hub.Adapters {
		adapters = append(adapters, &receiverAdaptersService.Adapter{
			Label:  adapter.Label().String(),
			Status: adapter.Status(),
		})
	}

	reply := receiverAdaptersService.ListReply{
		Adapters: adapters,
	}
	return &reply, nil
}
