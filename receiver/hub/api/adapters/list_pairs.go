package adapters

import (
	"context"
	"fmt"

	"gitlab.com/mwarnerdotme/opencap/receiver/adapters"

	receiverAdaptersService "gitlab.com/mwarnerdotme/opencap/receiver/hub/api/adapters/protos"
)

func (server ServiceServer) ListPairs(ctx context.Context, in *receiverAdaptersService.ListPairsRequest) (*receiverAdaptersService.ListPairsReply, error) {
	adapterType := adapters.ExchangeAdapterLabel(in.Label)
	_, exists := server.hub.Adapters[adapterType]

	if !exists {
		return nil, fmt.Errorf("adapter \"%s\" does not exist", in.Label)
	}

	var tradingPairs []*receiverAdaptersService.TradingPair

	for _, tradingPair := range server.hub.Adapters[adapterType].TradingPairs() {
		tradingPairs = append(tradingPairs, tradingPair)
	}

	reply := receiverAdaptersService.ListPairsReply{
		TradingPairs: tradingPairs,
	}
	return &reply, nil
}
