package adapters

import (
	"context"

	"gitlab.com/mwarnerdotme/opencap/receiver/adapters"

	receiverAdaptersService "gitlab.com/mwarnerdotme/opencap/receiver/hub/api/adapters/protos"
)

func (server ServiceServer) Start(ctx context.Context, in *receiverAdaptersService.StartRequest) (*receiverAdaptersService.StartReply, error) {
	adapterType := adapters.ExchangeAdapterLabel(in.Label)
	reply := receiverAdaptersService.StartReply{
		Success: false,
	}

	_, exists := server.hub.Adapters[adapterType]

	if exists {
		server.hub.Adapters[adapterType].Start()
		reply.Success = true
	}

	return &reply, nil
}
