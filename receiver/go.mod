module gitlab.com/mwarnerdotme/opencap/receiver

go 1.16

replace gitlab.com/mwarnerdotme/opencap/backend => ../backend

replace gitlab.com/mwarnerdotme/opencap/orchestrator => ../orchestrator

replace gitlab.com/mwarnerdotme/opencap/db => ../db

require (
	github.com/fasthttp/websocket v1.4.3
	github.com/valyala/fasthttp v1.22.0 // indirect
	gitlab.com/mwarnerdotme/opencap/backend v0.0.0-20210404185750-2e387e5f60b0
	gitlab.com/mwarnerdotme/opencap/orchestrator v0.0.0-00010101000000-000000000000
	google.golang.org/grpc v1.41.0
	google.golang.org/protobuf v1.25.0
)
