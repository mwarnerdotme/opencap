package main

import (
	"log"
	"os"

	"gitlab.com/mwarnerdotme/opencap/backend/env"
	"gitlab.com/mwarnerdotme/opencap/receiver/hub"
	"gitlab.com/mwarnerdotme/opencap/receiver/hub/api"
)

func main() {
	// ensure environment variables are defined
	var err error
	environment, found := os.LookupEnv("ENVIRONMENT")
	if !found {
		log.Fatal("ENVIRONMENT environment variable must be set")
	}
	if environment == "production" {
		err = env.EnsureRequiredEnvVarsAreDefined(".env.prod.example")
	}
	if environment != "production" {
		err = env.EnsureRequiredEnvVarsAreDefined(".env.example")
	}
	if err != nil {
		log.Fatal(err)
	}

	// initialize exchange adapter hub
	hub := hub.New()
	err = hub.Initialize()
	if err != nil {
		panic(err)
	}

	log.Println("Hub initialized")

	// initialize grpc API server
	api := api.New(hub)
	err = api.Initialize()
	if err != nil {
		panic(err)
	}

	log.Println("API initialized")
}
