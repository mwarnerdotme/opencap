package adapters

import (
	receiverAdaptersService "gitlab.com/mwarnerdotme/opencap/receiver/hub/api/adapters/protos"
)

type ExchangeAdapter interface {
	Start()
	Stop()
	Status() receiverAdaptersService.Adapter_Status
	Label() ExchangeAdapterLabel
	Refresh() error
	TradingPairs() map[string]*receiverAdaptersService.TradingPair
	Subscribe(tradingPairs []*receiverAdaptersService.TradingPair) (err error)
	Unsubscribe(tradingPairs []*receiverAdaptersService.TradingPair) (err error)
	ListConnections() []*receiverAdaptersService.Connection
}

type ExchangeAdapterLabel string

func (t ExchangeAdapterLabel) String() string {
	return string(t)
}
