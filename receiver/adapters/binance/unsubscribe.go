package binance

import (
	"strings"

	"github.com/fasthttp/websocket"

	receiverAdaptersService "gitlab.com/mwarnerdotme/opencap/receiver/hub/api/adapters/protos"
)

func (a *BinanceExchangeAdapter) unsubscribe(tradingPairs []tradingPair) (err error) {
	// start unsubscribing
	connsMap := make(map[*websocket.Conn][]string)
	for _, tradingPair := range tradingPairs {
		conn := a.tradingPairs[tradingPair]

		if conn == nil {
			continue
		}

		symbol := tradingPair.Symbol()
		param := strings.ToLower(symbol) + "@aggTrade"
		connsMap[conn] = append(connsMap[conn], param)

		a.tradingPairs[tradingPair] = nil
	}

	for conn, params := range connsMap {
		sm := subscribeMessage{
			Method: subscribeMethod_UNSUBSCRIBE,
			Params: params,
			ID:     1,
		}

		conn.WriteJSON(sm)
	}

	return
}

func (a *BinanceExchangeAdapter) Unsubscribe(tradingPairs []*receiverAdaptersService.TradingPair) (err error) {
	var pairs []tradingPair

	for _, tp := range tradingPairs {
		pair := tradingPair{
			BaseAsset:  tp.BaseAsset,
			QuoteAsset: tp.QuoteAsset,
		}
		pairs = append(pairs, pair)
	}

	err = a.unsubscribe(pairs)
	return
}
