package binance

import (
	"github.com/fasthttp/websocket"
	receiverAdaptersService "gitlab.com/mwarnerdotme/opencap/receiver/hub/api/adapters/protos"
)

func (a *BinanceExchangeAdapter) ListConnections() []*receiverAdaptersService.Connection {
	connections := []*receiverAdaptersService.Connection{}
	for conn := range a.websocketConnections {
		tradingPairs := []tradingPair{}
		for tradingPair, c := range a.tradingPairs {
			if c == conn {
				tradingPairs = append(tradingPairs, tradingPair)
			}
		}

		connectionTradingPairs := []*receiverAdaptersService.TradingPair{}
		for _, tradingPair := range tradingPairs {
			var status receiverAdaptersService.TradingPair_Status

			switch tradingPair.Status {
			case "TRADING":
				status = receiverAdaptersService.TradingPair_TRADING
			case "BREAK":
				status = receiverAdaptersService.TradingPair_NOT_TRADING
			}

			connectionTradingPairs = append(connectionTradingPairs, &receiverAdaptersService.TradingPair{
				BaseAsset:  tradingPair.BaseAsset,
				QuoteAsset: tradingPair.QuoteAsset,
				Status:     status,
			})
		}

		connections = append(connections, &receiverAdaptersService.Connection{
			Status:       receiverAdaptersService.Connection_CONNECTED,
			TradingPairs: connectionTradingPairs,
		})
	}

	return connections
}

func (a *BinanceExchangeAdapter) openConnection() (websocketConnection *websocket.Conn, err error) {
	websocketConnection, _, err = websocket.DefaultDialer.Dial(a.websocketApiUrl.String(), nil)
	if err != nil {
		a.handleError(err, "[BINANCE] - could not connect to the websocket API")
		return
	}
	a.websocketConnections[websocketConnection] = 0
	a.logger.Println("[BINANCE] - successfully opened a websocket connection")

	go a.handleIncomingMessages(websocketConnection)

	return
}

func (a *BinanceExchangeAdapter) closeConnection(conn *websocket.Conn) {
	conn.Close()
	delete(a.websocketConnections, conn)
}
