package binance

import (
	receiverAdaptersService "gitlab.com/mwarnerdotme/opencap/receiver/hub/api/adapters/protos"
)

func (a *BinanceExchangeAdapter) Status() receiverAdaptersService.Adapter_Status {
	return a.status
}
