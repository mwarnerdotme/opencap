package binance

import (
	"encoding/json"
	"fmt"
	"strconv"
	"strings"
	"time"

	"github.com/fasthttp/websocket"
	orchestratorTradesAPI "gitlab.com/mwarnerdotme/opencap/orchestrator/api/trades/protos"
	"gitlab.com/mwarnerdotme/opencap/receiver/adapters"
	"google.golang.org/protobuf/types/known/timestamppb"
)

/*
Read, parse, and execute incoming websocket messages.
*/
func (a *BinanceExchangeAdapter) handleIncomingMessages(websocketConnection *websocket.Conn) {
	defer func() {
		if r := recover(); r != nil {
			a.logger.Printf("[BINANCE] - websocket connection closed unexpectedly: %+v", r)
		}
	}()

	for {
		// read message
		_, message, err := websocketConnection.ReadMessage()
		if err != nil {
			continue
		}

		// handle trade messages
		if strings.Contains(string(message), "\"e\":\"aggTrade\"") {

			// parse message
			var tm tradeMessage
			err = json.Unmarshal(message, &tm)
			if err != nil {
				a.handleError(err, "could not unmarshal json")
				continue
			}

			go a.handleTradeMessage(tm)
		}
	}
}

/*
Handles an incoming websocket message for @agg/trade events.
*/
func (a *BinanceExchangeAdapter) handleTradeMessage(tm tradeMessage) {
	// parse price into float
	price, err := strconv.ParseFloat(tm.Price, 32)
	if err != nil {
		a.logger.Printf("could not parse price to float: %s", err)
		return
	}

	// parse trade quantity into float
	quantity, err := strconv.ParseFloat(tm.Quantity, 32)
	if err != nil {
		a.logger.Printf("could not parse quantity to float: %s", err)
		return
	}

	// parse trade time
	tradeTime := time.Unix(tm.TradeTime/1000, 0)
	executedAt := timestamppb.New(tradeTime)

	// send trade data to the adapter hub
	trade := &orchestratorTradesAPI.Trade{
		Exchange:   adapters.ExchangeAdapterType_BINANCE.String(),
		Symbol:     tm.Symbol,
		Price:      float32(price),
		Quantity:   float32(quantity),
		IsBuy:      tm.IsBuy,
		ExecutedAt: executedAt,
	}

	*a.AdapterTradeChannel <- trade
}

func (a *BinanceExchangeAdapter) handleError(err error, reason string) {
	errMessage := fmt.Errorf("[BINANCE] - %s: %s", reason, err)
	*a.AdapterErrorChannel <- errMessage
}
