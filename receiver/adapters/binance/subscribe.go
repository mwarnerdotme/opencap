package binance

import (
	"math"
	"strings"

	receiverAdaptersService "gitlab.com/mwarnerdotme/opencap/receiver/hub/api/adapters/protos"
)

const LIMIT = 200

func (a *BinanceExchangeAdapter) subscribe(tradingPairs []tradingPair) (err error) {
	// instantiate and populate slices for subscription parameters
	paramsMap := map[string]tradingPair{}

	for _, tp := range tradingPairs {
		existingConnection := a.tradingPairs[tp]

		if existingConnection != nil {
			continue
		}

		symbol := tp.Symbol()
		paramsMap[strings.ToLower(symbol)+"@aggTrade"] = tp
		// paramsMap[strings.ToLower(symbol)+"@depth"] = tp
	}

	// allocate websocket connections to send SUBSCRIBE messages to
	subscriptionAmount := len(paramsMap)
	totalConnectionAvailability := 0

	for _, subscriptions := range a.websocketConnections {
		availability := LIMIT - subscriptions
		totalConnectionAvailability += availability
	}

	if totalConnectionAvailability <= 0 || subscriptionAmount > totalConnectionAvailability {
		connectionsToOpen := float64((subscriptionAmount - totalConnectionAvailability)) / LIMIT

		for math.Ceil(connectionsToOpen) > 0 {
			a.openConnection()
			connectionsToOpen--
		}
	}

	// start subscribing
	for conn, subscriptions := range a.websocketConnections {
		availability := LIMIT - subscriptions

		if availability <= 0 || len(paramsMap) <= 0 {
			continue
		}

		params := make([]string, 0, availability)
		for param, tp := range paramsMap {
			// add trading pair to the list of parameters to subscribe to
			params = append(params, param)
			delete(paramsMap, param)

			// update cache on which connection is subscribed to this pair
			a.tradingPairs[tp] = conn

			// update availability
			availability--
			if availability <= 0 {
				break
			}
		}

		sm := subscribeMessage{
			Method: subscribeMethod_SUBSCRIBE,
			Params: params,
			ID:     1,
		}

		conn.WriteJSON(sm)
	}

	return
}

func (a *BinanceExchangeAdapter) Subscribe(tradingPairs []*receiverAdaptersService.TradingPair) (err error) {
	var pairs []tradingPair

	for _, tp := range tradingPairs {
		pair := tradingPair{
			BaseAsset:  tp.BaseAsset,
			QuoteAsset: tp.QuoteAsset,
		}
		pairs = append(pairs, pair)
	}

	err = a.subscribe(pairs)
	return
}
