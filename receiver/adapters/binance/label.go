package binance

import "gitlab.com/mwarnerdotme/opencap/receiver/adapters"

func (a *BinanceExchangeAdapter) Label() adapters.ExchangeAdapterLabel {
	return adapters.ExchangeAdapterType_BINANCE
}
