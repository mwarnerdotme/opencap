package binance

import (
	receiverAdaptersService "gitlab.com/mwarnerdotme/opencap/receiver/hub/api/adapters/protos"
)

func (a *BinanceExchangeAdapter) Stop() {
	a.logger.Println("[BINANCE] - closing all open websocket connections...")

	for conn := range a.websocketConnections {
		_ = conn.Close()
	}

	a.stop <- struct{}{}

	a = NewBinanceExchangeAdapter(a.AdapterTradeChannel, a.AdapterErrorChannel)

	a.logger.Println("[BINANCE] - all websocket connections were successfully closed")

	a.status = receiverAdaptersService.Adapter_STOPPED
}
