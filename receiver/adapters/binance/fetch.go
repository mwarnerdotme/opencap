package binance

import (
	"encoding/json"
	"io/ioutil"
	"net/http"

	receiverAdaptersService "gitlab.com/mwarnerdotme/opencap/receiver/hub/api/adapters/protos"
)

/*
Fetches and returns a slice of all available trading pairs on the exchange's market.
*/
func (a *BinanceExchangeAdapter) fetchAvailableTradingPairs() (tradingPairs []tradingPair, err error) {
	type exchangeInfoResponse struct {
		TradingPairs []tradingPair `json:"symbols"`
	}

	// make a REST API request to Binance for exchange information
	request, _ := http.NewRequest("GET", a.restApiUrl.String()+"/api/v3/exchangeInfo", nil)

	response, err := http.DefaultClient.Do(request)
	if err != nil {
		return
	}
	defer response.Body.Close()

	responseBody, err := ioutil.ReadAll(response.Body)
	if err != nil {
		return
	}

	var exchangeInfo exchangeInfoResponse
	err = json.Unmarshal(responseBody, &exchangeInfo)
	if err != nil {
		return
	}

	// build slice of available trading pairs
	tradingPairs = append(tradingPairs, tradingPairs...)

	return
}

func (a *BinanceExchangeAdapter) refreshTradingPairCache() (err error) {
	// fetch available trading pairs and symbols
	tradingPairs, err := a.fetchAvailableTradingPairs()
	if err != nil {
		a.handleError(err, "could not get available trading pairs")
		return
	}

	// cache available trading pairs
	for _, tradingPair := range tradingPairs {
		a.tradingPairs[tradingPair] = nil
	}

	return
}

func (a *BinanceExchangeAdapter) TradingPairs() map[string]*receiverAdaptersService.TradingPair {
	tradingPairs := make(map[string]*receiverAdaptersService.TradingPair)

	for tradingPair := range a.tradingPairs {
		var status receiverAdaptersService.TradingPair_Status

		switch tradingPair.Status {
		case "TRADING":
			status = receiverAdaptersService.TradingPair_TRADING
		case "BREAK":
			status = receiverAdaptersService.TradingPair_NOT_TRADING
		}

		tradingPairs[tradingPair.Symbol()] = &receiverAdaptersService.TradingPair{
			BaseAsset:  tradingPair.BaseAsset,
			QuoteAsset: tradingPair.QuoteAsset,
			Status:     status,
		}
	}

	return tradingPairs
}
