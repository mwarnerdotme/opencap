package binance

import (
	"log"

	receiverAdaptersService "gitlab.com/mwarnerdotme/opencap/receiver/hub/api/adapters/protos"
)

/*
Connects to the Binance websocket API and listens for market updates.
*/
func (a *BinanceExchangeAdapter) Start() {
	a.logger.Println("[BINANCE] - starting adapter...")

	err := a.refreshTradingPairCache()
	if err != nil {
		log.Println(err)
	}

	// handle server messages for stop
	go func() {
		for {
			select {
			case <-a.stop:
				a.logger.Println("stopping Binance adapter")
				close(a.stop)
				return
			}
		}
	}()

	a.status = receiverAdaptersService.Adapter_STARTED
}
