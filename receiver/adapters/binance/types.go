package binance

import (
	"log"
	"net/url"

	"github.com/fasthttp/websocket"
	orchestratorTradesAPI "gitlab.com/mwarnerdotme/opencap/orchestrator/api/trades/protos"
	"gitlab.com/mwarnerdotme/opencap/receiver/adapters"
	receiverAdaptersService "gitlab.com/mwarnerdotme/opencap/receiver/hub/api/adapters/protos"
)

/*
A trading pair as defined by the Binance REST API.
*/
type tradingPair struct {
	BaseAsset  string `json:"baseAsset"`
	QuoteAsset string `json:"quoteAsset"`
	Status     string `json:"status"`
}

func (t tradingPair) Symbol() string {
	return t.BaseAsset + t.QuoteAsset
}

/*
Binance exchange adapter.
*/
type BinanceExchangeAdapter struct {
	adapters.ExchangeAdapter
	status               receiverAdaptersService.Adapter_Status
	tradingPairs         tradingPairCache
	websocketConnections websocketConnectionCache
	restApiUrl           url.URL
	websocketApiUrl      url.URL
	logger               *log.Logger
	AdapterTradeChannel  *chan *orchestratorTradesAPI.Trade
	AdapterErrorChannel  *chan error
	stop                 chan struct{}
}

type subscribeMethod string

const subscribeMethod_SUBSCRIBE subscribeMethod = "SUBSCRIBE"
const subscribeMethod_UNSUBSCRIBE subscribeMethod = "UNSUBSCRIBE"

/*
Binance <- Journalist UN/SUBSCRIBE message.
*/
type subscribeMessage struct {
	Method subscribeMethod `json:"method"`
	Params []string        `json:"params"`
	ID     int             `json:"id"`
}

/*
Binance -> Journalist agg/Trade message.
*/
type tradeMessage struct {
	Symbol    string `json:"s"`
	Price     string `json:"p"`
	Quantity  string `json:"q"`
	TradeTime int64  `json:"T"`
	IsBuy     bool   `json:"m"`
}

/*
Cache for connected trading pairs (key is a trading pair instance).
*/
type tradingPairCache map[tradingPair]*websocket.Conn

/*
Cache for connected trading pairs (key is a pointer to a websocket connection).
*/
type websocketConnectionCache map[*websocket.Conn]int

/*
Creates and returns a new, unstarted instance of a Binance exchange adapter.
*/
func NewBinanceExchangeAdapter(tradeChannel *chan *orchestratorTradesAPI.Trade, errorChannel *chan error) *BinanceExchangeAdapter {
	a := &BinanceExchangeAdapter{}

	// add a new logger to the adapter
	a.logger = log.New(log.Writer(), log.Prefix(), log.Flags())

	// add api urls
	a.websocketApiUrl = url.URL{Scheme: "wss", Host: "stream.binance.com:9443", Path: "/ws"}
	a.restApiUrl = url.URL{Scheme: "https", Host: "api.binance.com"}

	// initialize caches
	a.tradingPairs = make(tradingPairCache)
	a.websocketConnections = make(websocketConnectionCache)

	a.AdapterTradeChannel = tradeChannel
	a.AdapterErrorChannel = errorChannel
	a.stop = make(chan struct{})

	a.status = receiverAdaptersService.Adapter_STOPPED

	return a
}
