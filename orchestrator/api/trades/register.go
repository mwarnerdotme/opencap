package trades

import (
	"context"
	"log"
	"time"

	"github.com/google/uuid"
	"github.com/prisma/prisma-client-go/runtime/transaction"
	"gitlab.com/mwarnerdotme/opencap/backend/types"
	"gitlab.com/mwarnerdotme/opencap/db"
	"gitlab.com/mwarnerdotme/opencap/orchestrator/api/trades/protos"
)

var tradePump []*types.Trade

type ServiceServer struct {
	protos.TradeServiceServer
}

func New(dbClient *db.PrismaClient) (s ServiceServer) {
	// start handling incoming trades
	go pump(dbClient)

	return
}

// persists trades in the trade pump to database
func pump(dbClient *db.PrismaClient) {
	for {
		if len(tradePump) > 0 {
			ops := []transaction.Param{}

			for _, trade := range tradePump {
				id := uuid.New().String()
				m := dbClient.Trades.CreateOne(
					db.Trades.ID.Set(id),
					db.Trades.Exchange.Set(trade.Exchange),
					db.Trades.Symbol.Set(trade.Symbol),
					db.Trades.Price.Set(trade.Price),
					db.Trades.Quantity.Set(trade.Quantity),
					db.Trades.IsBuy.Set(trade.IsBuy),
					db.Trades.ExecutedAt.Set(trade.ExecutedAt),
				).Tx()

				ops = append(ops, m)
			}

			err := dbClient.Prisma.Transaction(ops...).Exec(context.Background())
			if err != nil {
				log.Printf("could not insert trades into database: %s", err)
			}

			tradePump = []*types.Trade{}
		}

		time.Sleep(1 * time.Second)
	}
}
