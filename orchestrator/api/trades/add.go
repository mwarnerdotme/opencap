package trades

import (
	"io"
	"log"

	"github.com/shopspring/decimal"
	"gitlab.com/mwarnerdotme/opencap/backend/types"
	"gitlab.com/mwarnerdotme/opencap/orchestrator/api/trades/protos"
)

func (s ServiceServer) Add(server protos.TradeService_AddServer) (err error) {
	ctx := server.Context()

	for {
		// exit if context is done
		// or continue
		select {
		case <-ctx.Done():
			return ctx.Err()
		default:
		}

		// receive data from stream
		req, err := server.Recv()
		if err == io.EOF {
			// return will close stream from server side
			log.Println("exit")
			return nil
		}
		if err != nil {
			log.Printf("receive error %v", err)
			continue
		}

		// validate input
		if len(req.Trades) < 1 {
			server.Send(&protos.AddReply{
				Success: false,
			})
			continue
		}

		// translate in.Trades to (backend) types.Trade
		var trades []*types.Trade
		for _, protoTrade := range req.Trades {
			price := decimal.NewFromFloat32(protoTrade.Price)
			quantity := decimal.NewFromFloat32(protoTrade.Quantity)

			executedAt := protoTrade.ExecutedAt.AsTime()

			trade := &types.Trade{
				Exchange:   protoTrade.Exchange,
				Symbol:     protoTrade.Symbol,
				IsBuy:      protoTrade.IsBuy,
				Price:      price,
				Quantity:   quantity,
				ExecutedAt: executedAt,
			}

			trades = append(trades, trade)
		}

		tradePump = append(tradePump, trades...)
	}
}
