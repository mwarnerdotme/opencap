package api

import (
	"fmt"
	"net"

	"gitlab.com/mwarnerdotme/opencap/db"
	tradesService "gitlab.com/mwarnerdotme/opencap/orchestrator/api/trades"
	tradesProtos "gitlab.com/mwarnerdotme/opencap/orchestrator/api/trades/protos"
	"google.golang.org/grpc"
)

func Initialize(dbClient *db.PrismaClient) (err error) {
	// instantiate grpc server
	var opts []grpc.ServerOption
	grpcServer := grpc.NewServer(opts...)

	// init and register services
	tradesServiceServer := tradesService.New(dbClient)
	tradesProtos.RegisterTradeServiceServer(grpcServer, tradesServiceServer)

	// start grpc server
	listener, err := net.Listen("tcp", fmt.Sprintf(":%d", 9002))
	if err != nil {
		return
	}

	err = grpcServer.Serve(listener)
	if err != nil {
		return
	}

	return
}
