module gitlab.com/mwarnerdotme/opencap/orchestrator

go 1.16

replace gitlab.com/mwarnerdotme/opencap/backend => ../backend

replace gitlab.com/mwarnerdotme/opencap/db => ../db

require (
	google.golang.org/grpc v1.41.0
	google.golang.org/protobuf v1.25.0
)

require (
	github.com/google/uuid v1.1.2
	github.com/prisma/prisma-client-go v0.11.0
	github.com/shopspring/decimal v1.2.0
	gitlab.com/mwarnerdotme/opencap/backend v0.0.0-00010101000000-000000000000
	gitlab.com/mwarnerdotme/opencap/db v0.0.0-00010101000000-000000000000
)
