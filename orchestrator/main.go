package main

import (
	"log"
	"os"

	"gitlab.com/mwarnerdotme/opencap/backend/env"
	"gitlab.com/mwarnerdotme/opencap/db"
	"gitlab.com/mwarnerdotme/opencap/orchestrator/api"
)

func main() {
	// ensure environment variables are defined
	var err error
	environment, found := os.LookupEnv("ENVIRONMENT")
	if !found {
		log.Fatal("ENVIRONMENT environment variable must be set")
	}
	if environment == "production" {
		err = env.EnsureRequiredEnvVarsAreDefined(".env.prod.example")
	}
	if environment != "production" {
		err = env.EnsureRequiredEnvVarsAreDefined(".env.example")
	}
	if err != nil {
		log.Fatal(err)
	}

	// connect to timescale
	dbClient := db.NewClient()
	err = dbClient.Prisma.Connect()
	if err != nil {
		panic(err)
	}

	// start local grpc server to listen for trade data
	err = api.Initialize(dbClient)
	if err != nil {
		panic(err)
	}
}
