# OpenCap Orchestrator Service

## Getting Started

### Prerequisites

* Running TimescaleDB server (Postgres + TimescaleDB plugin)
    * With current schema - see `/db/README.md`

### Starting up

```bash
cp .env.example .env
# ... make edits ...

source .env && go run .
```

This will start a new Orchestrator server. Orchestrator has a gRPC streaming endpoint for incoming trades. It uses this stream connection to know which Receivers it can communicate with. Receivers\* have several methods on the `AdapterService` that allow them to be configured without being restarted. Orchestrator, knowing how to communicate with the connected Receiver, can start querying against

> \* See `/receivers/README.md` for more information on Receivers.

### Receiving trade data

To begin storing trades in a database, start up a Receiver and point it to your running Orchestrator. Once some trading pairs have been subscribed to and trades are coming in, they will be forwarded to Orchestrator and get stored in the database!

### Managing Receivers

As mentioned above, Orchestrator provides gRPC endpoints for inspecting and managing Receivers. With the Next.js powered admin dashboard (`/frontend/admin` - coming soon!), you have access to a user-friendly interface for configuring all of the Receivers that you're running, as well as Orchestrator itself.

## Purpose

To centralize all incoming trade data and management of trade data Receivers.
