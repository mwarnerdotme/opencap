package types

import (
	"gitlab.com/mwarnerdotme/opencap/db"
)

type Trade db.InnerTrades

type TradingPair interface {
	Symbol() string
}
