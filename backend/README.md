# OpenCap Backend Utils

## What's in this package?

This package contains some shared utilities for backend use.

`env` is used to manage .env file dependencies.

`services` houses wrapper logic for third-party services like Redis (originally Postgres as well before Prisma was implemented). This could be used for logging and other "action" services in the future.

`types` is a central location for types that are shared between Orchestrator and Receiver.