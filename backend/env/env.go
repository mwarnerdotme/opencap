package env

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"regexp"
	"strconv"
	"strings"
)

var filepath string
var requiredEnvironmentVariableKeys []string

func EnsureRequiredEnvVarsAreDefined(envExampleFilepath string) error {
	filepath = envExampleFilepath
	// get slice of required keys from .env.example
	requiredEnvironmentVariableKeys = getKeysFromEnvVarExampleFile(envExampleFilepath)

	// check to see that the variables in .env.example are present in .env
	missingKeys := []string{}
	for _, key := range requiredEnvironmentVariableKeys {
		if _, found := os.LookupEnv(key); !found {
			missingKeys = append(missingKeys, key)
		}
	}

	if len(missingKeys) > 0 {
		concatenatedMissingKeys := strings.Join(missingKeys, ", ")
		return fmt.Errorf("These environment variables must be set before starting: %s", concatenatedMissingKeys)
	}

	return nil
}

func Getenv(key string) string {
	isRequired := isKeyRequired(key)
	if !isRequired {
		log.Printf("'%s' environment varaible was used in the code but not found in %s as a requirement. If this is a required variable in this environment, please add it to the appropriate example file.", key, filepath)
	}
	return os.Getenv(key)
}

func GetenvInt(key string) int {
	value := Getenv(key)
	convertedValue, _ := strconv.Atoi(value)
	return convertedValue
}

func GetenvFloat(key string) float64 {
	value := Getenv(key)
	convertedValue, _ := strconv.ParseFloat(value, 64)
	return convertedValue
}

func isKeyRequired(key string) bool {
	for _, k := range requiredEnvironmentVariableKeys {
		if k == key {
			return true
		}
	}
	return false
}

func getKeysFromEnvVarExampleFile(filepath string) []string {
	// read file
	f, err := os.Open(filepath)
	if err != nil {
		log.Fatalf("Could not read \"%s\" to determine required environment variables: %v", filepath, err)
	}
	defer f.Close()

	// parse and append keys to slice
	var exampleFileKeys []string = []string{}
	scanner := bufio.NewScanner(f)
	expression := regexp.MustCompile(`export\s(.*)=(.*)`)
	for scanner.Scan() {
		line := scanner.Text()
		if line != "" && line != "\n" {
			capture := expression.FindStringSubmatch(line)
			if len(capture) > 0 {
				exampleFileKeys = append(exampleFileKeys, capture[1])
			}
		}
	}

	if err := scanner.Err(); err != nil {
		log.Fatalf("Could not read \"%s\" to determine required environment variables: %v", filepath, err)
	}

	// return
	return exampleFileKeys
}
