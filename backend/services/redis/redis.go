package redis

import (
	"fmt"
	"strconv"

	"github.com/go-redis/redis"
	"gitlab.com/mwarnerdotme/opencap/backend/env"
)

var RedisClient *redis.Client

func StartRedisClient() (err error) {
	db, _ := strconv.Atoi(env.Getenv("REDIS_DB"))

	addr := fmt.Sprintf("%s:%s", env.Getenv("REDIS_HOST"), env.Getenv("REDIS_PORT"))
	client := redis.NewClient(&redis.Options{
		Addr: addr,
		DB:   db,
	})
	err = client.Ping().Err()
	if err != nil {
		return
	}

	RedisClient = client
	return
}
