module gitlab.com/mwarnerdotme/opencap/backend

go 1.17

replace gitlab.com/mwarnerdotme/opencap/db => ../db

require (
	github.com/go-redis/redis v6.15.9+incompatible
	github.com/onsi/ginkgo v1.15.2 // indirect
	github.com/onsi/gomega v1.11.0 // indirect
	gitlab.com/mwarnerdotme/opencap/db v0.0.0-00010101000000-000000000000
)

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/iancoleman/strcase v0.2.0 // indirect
	github.com/joho/godotenv v1.4.0 // indirect
	github.com/prisma/prisma-client-go v0.11.0 // indirect
	github.com/shopspring/decimal v1.2.0 // indirect
	github.com/stretchr/testify v1.7.0 // indirect
	github.com/takuoki/gocase v1.0.0 // indirect
	golang.org/x/net v0.0.0-20210226172049-e18ecbb05110 // indirect
	golang.org/x/sys v0.0.0-20210615035016-665e8c7367d1 // indirect
	golang.org/x/text v0.3.7 // indirect
)
