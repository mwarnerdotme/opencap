# OpenCap

## What's OpenCap?

OpenCap is an open source cryptocurrency exchange aggregate. It's a set of microservices designed to listen to incoming market data from popular exchanges and store it in a time-series database. OpenCap ships with a default browser interface for viewing data, but you could also use the data for:

- Data analysis
- Machine learning
- Hosting a public or private price index API

## Getting Started

To run OpenCap locally, you'll need to first run an Orchestrator. More information can be found in `/orchestrator/README.md`.

Once Orchestrator is running, you'll need to run a Receiver microservice which can be configured and managed via the Orchestrator's admin frontend (coming soon!). See `/frontend/admin` for more details.

From there, you'll be able to start up some exchange adapters on the Receiver and the data should start flowing to Orchestrator (and then into a TimescaleDB database). You've got yourself a running dataset!

## Completing Your OpenCap Dataset

Coming soon!

## Contributing

See `CONTRIBUTING.md`

## Technologies

- Frontend
  - [Next.js](https://nextjs.org)
  - [GraphQL](https://graphql.org) & [Apollo](https://graphqlapollo.com)
  - [Next.js]()
- Backend
  - [fasthttp/websocket](https://github.com/fasthttp/websocket)
  - [Prisma](https://prisma.io/)
  - [gRPC](https://grpc.io/)
