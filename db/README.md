# OpenCap Database Service

## Getting Started

### Prerequisites

* Node.js v12 or higher installed
* Running TimescaleDB server (Postgres + TimescaleDB plugin)
  * Use `timescale/timescaledb:latest-pg12` Docker image to start

### Using your local database

```bash
cp ./.env.example .env
# ...make edits...
```

### Migrations

After initially pulling this repository, run this command to set up your database and generate the necessary database clients to run OpenCap Orchestrator and the frontends.

```bash
npx prisma migrate dev
```

> If you change the `prisma/schema.prisma` file at all in development, you will need to run this command again. If the new schema is accurate, it will automatically create a new migration (see `prisma/migrations`), run the new migration, and generate the new Golang and Node clients.

### Regenerating Prisma clients

The previous migration command should take care of this, but that command is currently listed as experimental by Prisma. It almost always works without any issues, but in case you run into anything, you can set up the database manually (see the `prisma/schema.prisma` file for reference) and run this to get a client for both Golang and Node/Typescript.

```bash
npx prisma generate
```

## Why Prisma/what's happening here?

Type safety across the stack, as well as easy of use. The schema gets updated once (`/db/prisma/schema.prisma`) and the resulting clients are used by both the Golang backend and the Next.js frontend.
