/*
  Warnings:

  - Made the column `is_buy` on table `trades` required. This step will fail if there are existing NULL values in that column.
  - Made the column `created_at` on table `trades` required. This step will fail if there are existing NULL values in that column.

*/
-- AlterTable
ALTER TABLE "trades" ALTER COLUMN "is_buy" SET NOT NULL,
ALTER COLUMN "created_at" SET NOT NULL,
ALTER COLUMN "created_at" SET DEFAULT CURRENT_TIMESTAMP;
