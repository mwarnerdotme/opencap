/*
  Warnings:

  - A unique constraint covering the columns `[id,executed_at]` on the table `trades` will be added. If there are existing duplicate values, this will fail.

*/
-- CreateIndex
CREATE UNIQUE INDEX "trades_id_executed_at_key" ON "trades"("id", "executed_at");
