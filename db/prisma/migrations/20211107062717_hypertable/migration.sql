-- TimescaleDB
CREATE EXTENSION IF NOT EXISTS timescaledb CASCADE;

-- CreateTable
CREATE TABLE "trades" (
    "id" TEXT NOT NULL,
    "exchange" VARCHAR(20) NOT NULL,
    "symbol" VARCHAR(15) NOT NULL,
    "price" DECIMAL NOT NULL,
    "quantity" DECIMAL NOT NULL,
    "is_buy" BOOLEAN,
    "executed_at" TIMESTAMPTZ(6) NOT NULL,
    "created_at" TIMESTAMPTZ(6)
);

-- CreateIndex
CREATE INDEX "idx_trades_exchange" ON "trades"("exchange");

-- CreateIndex
CREATE INDEX "idx_trades_executed_at" ON "trades"("executed_at");

-- TimescaleDB - Create HyperTable
SELECT create_hypertable('trades', 'executed_at');
